package com.x.wcrm.assemble.control.jaxrs.config;

import com.google.gson.JsonElement;
import com.x.base.core.container.EntityManagerContainer;
import com.x.base.core.container.factory.EntityManagerContainerFactory;
import com.x.base.core.entity.JpaObject;
import com.x.base.core.entity.annotation.CheckPersistType;
import com.x.base.core.project.bean.WrapCopier;
import com.x.base.core.project.bean.WrapCopierFactory;
import com.x.base.core.project.http.ActionResult;
import com.x.base.core.project.http.EffectivePerson;
import com.x.base.core.project.logger.Logger;
import com.x.base.core.project.logger.LoggerFactory;
import com.x.wcrm.assemble.control.jaxrs.leads.BaseAction;
import com.x.wcrm.core.entity.WCrmConfig;
import org.apache.commons.lang3.StringUtils;

public class  ActionUpdate extends BaseAction {

	private static Logger logger = LoggerFactory.getLogger(ActionUpdate.class);

	ActionResult<Wo> execute(EffectivePerson effectivePerson, String id, JsonElement jsonElement) throws Exception {
		try (EntityManagerContainer emc = EntityManagerContainerFactory.instance().create()) {
			ActionResult<Wo> result = new ActionResult<>();
			Wi wi = this.convertToWrapIn(jsonElement, Wi.class);
			WCrmConfig o = emc.find(id, WCrmConfig.class);
			String _id = o.getId();
			// 更新数据
				Wi.copier.copy(wi, o);

			if (null == o.getId() || StringUtils.isBlank(o.getId())) {
				logger.info("ActionUpdate set id:" + _id);
				o.setId(_id);
			}

			emc.beginTransaction(WCrmConfig.class);
			emc.persist(o, CheckPersistType.all);
			emc.commit();

			Wo wo = new Wo();
			Wo.copier.copy(o,wo);
			wo.setId(o.getId());
			result.setData(wo);

			return result;
		}
	}

	static class Wi extends WCrmConfig {
		private static final long serialVersionUID = -4714395467753481398L;
		static WrapCopier<Wi, WCrmConfig> copier = WrapCopierFactory.wi(Wi.class, WCrmConfig.class, null, JpaObject.FieldsUnmodify, true);
	}

	public static class Wo extends WCrmConfig {
		private static final long serialVersionUID = 7871578639804765941L;
		static WrapCopier<WCrmConfig, Wo> copier = WrapCopierFactory.wo(WCrmConfig.class, Wo.class, null, JpaObject.FieldsUnmodify);
	}
}
